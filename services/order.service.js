const { Orders, OrdersFoods, Users, Foods, restaurants } = require("../models");
const moment = require("moment");
const { Op } = require("sequelize");
const { sequelize } = require("../models");
const {
  deleteOrdersFoodsByOrderIdService,
} = require("./orders_foods.services");
const createOrderService = async ({ userId }) => {
  const newOrder = await Orders.create({ userId });
  return newOrder;
};
const deleteOrderByIdService = async (id) => {
  await Orders.destroy({
    where: {
      id,
    },
  });
};
const getOrderByOrderIdService = async (id) => {
  const order = await Orders.findOne({
    where: {
      id,
    },
  });
  if (order) {
    return order;
  } else {
    return false;
  }
};
const getOrderDetailByIdService = async (id) => {
  const orderQuery = await Orders.findOne({
    attributes: ["id", "createdAt", "status"],
    where: { id },
    required: true,
    include: [
      {
        model: OrdersFoods,
        attributes: ["quantityFood"],
        required: true,
        include: [
          {
            model: Foods,
            required: true,
            attributes: ["price", "name", "restaurantId"],
            include: [
              {
                model: restaurants,
                attributes: ["name", "id"],
                required: true,
              },
            ],
          },
        ],
      },
      {
        model: Users,
        attributes: ["fullName"],
      },
    ],
  });
  let restaurant = "";
  if (orderQuery.OrdersFoods.length !== 0) {
    restaurant = orderQuery.OrdersFoods[0].Food.restaurant.name;
  }
  const order = {
    status: orderQuery.status,
    orderId: orderQuery.id,
    fullname: orderQuery.User.fullName,
    timeOrder: orderQuery.createdAt,
    restaurant,
  };
  const foodList = orderQuery.OrdersFoods.map((item) => {
    return {
      quantityFood: item.quantityFood,
      foodName: item.Food.name,
      price: item.Food.price,
    };
  });
  return { ...order, foodList };
};
const getOrdersService = async (
  restaurantId,
  userId,
  userName,
  currentPage,
  perPage,
  orderStatus,
  orderDate,
) => {
  const conditionOrderDate = orderDate
    ? {
        createdAt: sequelize.where(
          sequelize.fn("date", sequelize.col("Orders.createdAt")),
          "=",
          moment(orderDate).format("YYYY-MM-DD"),
        ),
      }
    : null;
  const conditionOrderStatus =
    orderStatus === undefined ? null : { status: orderStatus };
  console.log("conditionOrderStatus: ", conditionOrderStatus);
  const conditionUserId = userId ? { id: userId } : null;
  const conditionUserName = userName
    ? { fullname: { [Op.like]: `%${userName}%` } }
    : null;
  const conditionRestaurant = restaurantId
    ? { restaurantId: restaurantId }
    : null;
  const queryOrders = await Orders.findAll({
    where: {
      ...conditionOrderStatus,
      ...conditionOrderDate,
    },

    attributes: ["id", "createdAt", "status"],
    required: true,

    offset: (currentPage * 1 - 1) * perPage,
    limit: perPage * 1,
    order: [["id", "DESC"]],
    subQuery: false,
    include: [
      {
        model: OrdersFoods,
        attributes: ["quantityFood"],
        required: true,
        include: [
          {
            model: Foods,
            required: true,
            where: {
              ...conditionRestaurant,
            },
            attributes: ["price", "name", "restaurantId"],
            include: [
              {
                model: restaurants,

                attributes: ["name", "id"],
                required: true,
              },
            ],
          },
        ],
      },
      {
        model: Users,
        required: true,
        where: {
          ...conditionUserId,
          ...conditionUserName,
        },
        attributes: ["fullName"],
      },
    ],
  });
  const orderLength = await Orders.count();

  const orders = queryOrders.map((item) => {
    let restaurant = "";
    if (item.OrdersFoods.length !== 0) {
      restaurant = item.OrdersFoods[0].Food.restaurant.name;
    }
    const order = {
      status: item.status,
      orderId: item.id,
      fullname: item.User.fullName,
      timeOrder: item.createdAt,
      restaurant,
    };

    const foodList = item.OrdersFoods.map((item) => {
      return {
        quantityFood: item.quantityFood,
        foodName: item.Food.name,
        price: item.Food.price,
      };
    });
    return { ...order, foodList };
  });
  return { orders, orderLength };
};
const updateOrderService = async (order, id) => {
  const orderUpdate = await getOrderByOrderIdService(id);
  for (const key in order) {
    orderUpdate[key] = order[key];
  }
  await orderUpdate.save();
  return orderUpdate;
};
const deleteOrderByUserIdService = async (userId) => {
  const orders = await Orders.findAll({ where: { userId } });
  for (const order of orders) {
    await deleteOrdersFoodsByOrderIdService(order.id);
    await deleteOrderByIdService(order.id);
  }
};
module.exports = {
  createOrderService,
  deleteOrderByIdService,
  getOrderByOrderIdService,
  getOrderDetailByIdService,
  getOrdersService,
  updateOrderService,
  deleteOrderByUserIdService,
};
