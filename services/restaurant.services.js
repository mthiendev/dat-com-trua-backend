const { restaurants } = require("../models");
const {
  getFoodsByIdRestaurants,
  deleteFoodByRestaurantIdService,
} = require("./food.service");
const { Op } = require("sequelize");
// @TODO
// lay dk tu query params
// check dk null thi k bo vao findAll
// co dk thi bo vao findALl
//
const getAllRestaurantsService = async (queryParams) => {
  const where = {};

  if (queryParams && queryParams.name) {
    where.name = {
      [Op.like]: `%${queryParams.name.trim()}%`,
    };
  }
  const restaurantList = await restaurants.findAll({
    where: where,
  });
  return restaurantList;
};

const getRestaurantListService = async () => {
  const restaurantList = await getAllRestaurantsService();

  const result = [];
  for (const restaurant of restaurantList) {
    const foods = await getFoodsByIdRestaurants(restaurant.id);
    const newRestaurant = {
      id: restaurant.id,
      name: restaurant.name,
      address: restaurant.address,
      phoneNumber: restaurant.phoneNumber,
      foodList: foods,
    };
    result.push(newRestaurant);
  }
  return result;
};

const getRestaurantByIdService = async (id) => {
  const restaurant = restaurants.findOne({ where: { id } });
  return restaurant;
};

const deleteRestaurantByIdService = async (id) => {
  await deleteFoodByRestaurantIdService(id);
  await restaurants.destroy({ where: { id } });
};

const updateRestaurantByIdService = async (restaurant, id) => {
  try {
    const restaurantUpdate = await getRestaurantByIdService(id);
    if (restaurantUpdate) {
      for (const info in restaurantUpdate.dataValues) {
        restaurantUpdate[info] = restaurant[info];
      }
      await restaurantUpdate.save();
      return restaurantUpdate;
    }
  } catch (error) {
    throw new Error("Dữ liệu nhà hàng không hợp lệ");
  }

  return null;
};
module.exports = {
  getRestaurantByIdService,
  getAllRestaurantsService,
  getRestaurantListService,
  deleteRestaurantByIdService,
  updateRestaurantByIdService,
};
