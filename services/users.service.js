const { Users } = require("../models");
const bcryptjs = require("bcryptjs");
const { deleteOrderByUserIdService } = require("./order.service");
const { Op } = require("sequelize");

const getAllUsersService = async () => {
  const usersList = await Users.findAll();
  return usersList;
};
const registerService = async (
  fullName,
  email,
  department,
  password,
  phoneNumber
) => {
  const userExist = await Users.findOne({
    where: { email },
  });
  if (userExist) {
    return { error: "Email đã tồn tại" };
  }
  const salt = bcryptjs.genSaltSync(10);
  const hashPassword = bcryptjs.hashSync(password, salt);
  const newUser = await Users.create({
    fullName,
    password: hashPassword,
    phoneNumber,
    email,
    department,
  });
  return { newUser };
};

const getAllUserPaginationService = async (offSet) => {
  const userListPaginated = await Users.findAndCountAll({
    offset: offSet,
    limit: 4,
  });
  return userListPaginated;
};

const getListSearchBaseOnInfoService = async (info, offset) => {
  const listSearch = await Users.findAndCountAll({
    where: {
      [Op.or]: [
        {
          fullName: {
            [Op.like]: `%${info.trim().toLowerCase()}%`,
          },
        },
        {
          phoneNumber: {
            [Op.like]: `%${info.trim().toLowerCase()}%`,
          },
        },
        {
          email: {
            [Op.like]: `%${info.trim().toLowerCase()}%`,
          },
        },
        {
          type: {
            [Op.like]: `%${info.trim().toLowerCase()}%`,
          },
        },
        {
          department: {
            [Op.like]: `%${info.trim().toLowerCase()}%`,
          },
        },
      ],
    },
    offset,
    limit: 4,
  });

  return listSearch;
};

const getUserInfoByIdService = async (id) => {
  const user = Users.findOne({ where: { id } });
  return user;
};

const deleteUserByIdService = async (id) => {
  await deleteOrderByUserIdService(id);
  await Users.destroy({ where: { id } });
};
const updateUserInfoByIdService = async (newUserInfo, id) => {
  const salt = bcryptjs.genSaltSync(10);
  const hashPassword = bcryptjs.hashSync(newUserInfo.password, salt);
  const oldUserInfo = await getUserInfoByIdService(id);
  if (oldUserInfo.email !== newUserInfo.email) {
    const userExist = await Users.findOne({
      where: { email: newUserInfo.email },
    });
    if (userExist) {
      return { error: "email đã tồn tại" };
    }
  }
  if (oldUserInfo) {
    for (const info in oldUserInfo.dataValues) {
      oldUserInfo[info] = newUserInfo[info];
      if (info === "password") {
        oldUserInfo[info] = hashPassword;
      }
    }

    await oldUserInfo.save();
    return { userUpdate: oldUserInfo };
  }
  return null;
};

module.exports = {
  getAllUsersService,
  deleteUserByIdService,
  updateUserInfoByIdService,
  getUserInfoByIdService,
  registerService,
  getAllUserPaginationService,
  getListSearchBaseOnInfoService,
};
