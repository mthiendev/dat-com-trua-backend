const { Foods } = require("../models");
const { deleteOrderFoodByFoodIdService } = require("./orders_foods.services");
const getFoodsByIdRestaurants = async (restaurantId) => {
  const foods = await Foods.findAll({
    where: {
      restaurantId,
    },
  });
  if (foods) {
    return foods;
  }
  return [];
};
const createFoodService = async ({ name, quantity, price, restaurantId }) => {
  const newRestaurant = await Foods.create({
    name,
    quantity,
    price,
    restaurantId,
  });
  return newRestaurant;
};
const getAllFoodService = async () => {
  const foodtList = await Foods.findAll();
  return foodtList;
};
const getFoodByIdService = async (id) => {
  const food = await Foods.findOne({
    where: {
      id,
    },
  });
  return food;
};
const deleFoodByIdServices = async (id) => {
  if (!id) return { status: false, error: "Id invalid" };
  await deleteOrderFoodByFoodIdService(id);
  await Foods.destroy({
    where: { id },
  });
  return { status: true, error: "" };
};
const updateFoodServices = async (food, id) => {
  const foodUpdate = await getFoodByIdService(id);
  if (foodUpdate) {
    foodUpdate.name = food.name;
    foodUpdate.price = food.price;
    foodUpdate.restaurantId = food.restaurantId;
    foodUpdate.quantity = food.quantity;
    await foodUpdate.save();
    return foodUpdate;
  }
  return null;
};
const deleteFoodByRestaurantIdService = async (restauRantId) => {
  if (!restauRantId) return { status: false, error: "Id invalid" };
  const foods = await getFoodsByIdRestaurants(restauRantId);
  for (const food of foods) {
    await deleFoodByIdServices(food.id);
  }
  await Foods.destroy({
    where: {
      restauRantId,
    },
  });
  return { status: true, error: "" };
};
module.exports = {
  deleteFoodByRestaurantIdService,
  getFoodsByIdRestaurants,
  createFoodService,
  getAllFoodService,
  getFoodByIdService,
  deleFoodByIdServices,
  updateFoodServices,
};
