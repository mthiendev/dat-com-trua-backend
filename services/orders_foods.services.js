const { OrdersFoods } = require("../models");
const { Foods } = require("../models");
const createOrderFoodService = async ({ orderId, foodId, quantityFood }) => {
  console.log("quantityFood: ", quantityFood);
  console.log("foodId: ", foodId);
  console.log("orderId: ", orderId);

  const foodUpdate = await Foods.findOne({
    where: {
      id: foodId,
    },
  });

  console.log("foodUpdate: ", foodUpdate);
  if (!foodUpdate) {
    res.status(404).send("ID invalid");
    return;
  }
  if (foodUpdate.quantity < quantityFood) {
    return { newOrderFood: null, err: "Số lượng món còn lại không đủ" };
  } else {
    foodUpdate.quantity = foodUpdate.quantity - quantityFood;
    await foodUpdate.save();
  }
  const newOrderFood = await OrdersFoods.create({
    orderId,
    foodId,
    quantityFood,
  });
  console.log("newOrderFood: ", newOrderFood);
  return { newOrderFood, err: "" };
};
const deleteOrdersFoodsService = async (id) => {
  await OrdersFoods.destroy({
    where: {
      id,
    },
  });
};
const deleteOrdersFoodsByOrderIdService = async (orderId) => {
  if (!orderId) return { status: false, error: "Id invalid" };
  await OrdersFoods.destroy({
    where: {
      orderId,
    },
  });
  return { status: true, error: "" };
};
const deleteOrderFoodByFoodIdService = async (foodId) => {
  await OrdersFoods.destroy({
    where: {
      foodId,
    },
  });
};
module.exports = {
  deleteOrderFoodByFoodIdService,
  createOrderFoodService,
  deleteOrdersFoodsService,
  deleteOrdersFoodsByOrderIdService,
};
