'use strict';
const { Model } = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class OrdersFoods extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate({ Orders, Foods }) {
      this.belongsTo(Orders, { foreignKey: 'orderId' });
      this.belongsTo(Foods, { foreignKey: 'foodId' });
    }
  }
  OrdersFoods.init(
    {
      quantityFood: { type: DataTypes.INTEGER },
    },
    {
      sequelize,
      modelName: 'OrdersFoods',
    },
  );
  return OrdersFoods;
};
