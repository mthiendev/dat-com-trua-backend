'use strict';
const { Model } = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Foods extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate({ restaurants, OrdersFoods }) {
      // define association here
      this.belongsTo(restaurants, { foreignKey: 'restaurantId' });
      this.hasMany(OrdersFoods, { foreignKey: 'foodId' });
    }
  }
  Foods.init(
    {
      name: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      price: {
        type: DataTypes.FLOAT,
        allowNull: false,
      },
      quantity: {
        type: DataTypes.INTEGER,
        defaultValue: 20,
      },
    },
    {
      sequelize,
      modelName: 'Foods',
    },
  );
  return Foods;
};
