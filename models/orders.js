"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class Orders extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate({ Users, OrdersFoods }) {
      // define association here
      this.belongsTo(Users, { foreignKey: "userId" });
      this.hasMany(OrdersFoods, { foreignKey: "orderId" });
    }
  }
  Orders.init(
    {
      userId: DataTypes.INTEGER,
      status: { type: DataTypes.BOOLEAN, defaultValue: false },
    },
    {
      sequelize,
      modelName: "Orders",
    },
  );
  return Orders;
};
