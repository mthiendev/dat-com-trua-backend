const {
  createOrderService,
  deleteOrderByIdService,
  getOrdersService,
  updateOrderService,
  getOrderByOrderIdService,
  getOrderDetailByIdService,
} = require("../services/order.service");
const {
  deleteOrdersFoodsByOrderIdService,
} = require("../services/orders_foods.services");
const createOrder = async (req, res) => {
  const { id } = req.user;
  try {
    if (id) {
      const newOrder = await createOrderService({ userId: id });
      res.status(201).send(newOrder);
    } else {
      res.status(404).send("ID invalid !");
    }
  } catch (error) {
    res.status(500).send(error);
  }
};
const deleteOrder = async (req, res) => {
  const { id } = req.params;
  try {
    await deleteOrdersFoodsByOrderIdService(id);
    await deleteOrderByIdService(id);
    res.status(200).send("Xóa thành công");
  } catch (error) {
    res.status(500).send(error);
  }
};
const getOrders = async (req, res) => {
  const {
    restaurantId,
    userId,
    userName,
    currentPage,
    perPage,
    orderStatus,
    orderDate,
  } = req.query;
  try {
    const results = await getOrdersService(
      restaurantId,
      userId,
      userName,
      currentPage,
      perPage,
      orderStatus,
      orderDate,
    );

    if (results) {
      res.status(200).send(results);
    }
  } catch (error) {
    res.status(500).send(error);
  }
};
const getOrdersByOrderId = async (req, res) => {
  const { id } = req.params;
  try {
    const results = await getOrderDetailByIdService(Number(id));
    res.status(200).send(results);
  } catch (error) {
    res.status(500).send(error);
  }
};
const updateOrderStatus = async (req, res) => {
  const { id } = req.params;
  console.log("id: ", id);

  const order = req.body;
  console.log("order: ", order);
  try {
    const orderUpdate = await updateOrderService(order, id);
    res.status(200).send(orderUpdate);
  } catch (error) {
    res.status(500).send(error);
  }
};
module.exports = {
  createOrder,
  deleteOrder,
  getOrders,
  getOrdersByOrderId,
  updateOrderStatus,
};
