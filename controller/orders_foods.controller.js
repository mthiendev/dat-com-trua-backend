const { createOrderService } = require("../services/order.service");
const {
  createOrderFoodService,
  deleteOrdersFoodsService,
} = require("../services/orders_foods.services");
const createOrdersFoods = async (req, res) => {
  const userId = req.user.id;

  const ordersFoods = req.body;
  console.log("ordersFoods: ", ordersFoods);
  try {
    if (!userId) {
      res.status(404).send("ID invalid");
      return;
    }
    const newOrder = await createOrderService({ userId });

    for (const food of ordersFoods) {
      const { newOrderFood, err } = await createOrderFoodService({
        ...food,
        orderId: newOrder.id,
      });

      if (!newOrderFood) {
        res.status(207).send(err);
        return;
      }
    }
    res.status(200).send("Đặt hàng thành công !");
  } catch (error) {
    res.status(500).send(error);
  }
};
const deleteOrdersFoods = async (req, res) => {
  const { id } = req.params;
  try {
    await deleteOrdersFoodsService(id);
    res.status(200).send("Xóa thành công");
  } catch (error) {
    res.status(500).send(error);
  }
};
module.exports = {
  createOrdersFoods,
  deleteOrdersFoods,
};
