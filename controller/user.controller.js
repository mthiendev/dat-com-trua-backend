const { Users } = require("../models");
const bcryptjs = require("bcryptjs");
const jwt = require("jsonwebtoken");
const {
  getAllUsersService,
  deleteUserByIdService,
  updateUserInfoByIdService,
  getUserInfoByIdService,
  registerService,
  getAllUserPaginationService,
  getListSearchBaseOnInfoService,
} = require("../services/users.service");
require("dotenv").config();
const { validationResult } = require("express-validator");

const register = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.status(422).json({ errors: errors.array() });
    return;
  }
  const { fullName, email, department, password, phoneNumber } = req.body;
  try {
    const result = await registerService(
      fullName,
      email,
      department,
      password,
      phoneNumber
    );
    if (result.newUser) {
      res.status(201).send(result.newUser);
    } else {
      res.status(401).send({ errors: [{ msg: result.error }] });
    }
  } catch (error) {
    res.status(500).send(error);
  }
};
const login = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.status(422).json({ errors: errors.array() });
    return;
  }
  const { email, password } = req.body;
  console.log("email: ", email);
  try {
    const user = await Users.findOne({
      where: { email },
    });
    if (user) {
      const isAuth = bcryptjs.compareSync(password, user.password);
      if (isAuth) {
        const token = jwt.sign(
          {
            email: user.email,
            type: user.type,
            id: user.id,
          },
          process.env.SECRETKEY
        );
        res.status(200).send({ message: "Đăng nhập thành công!", token, user });
      } else {
        res.status(500).send({ message: "Tài khoản hoặc mật khẩu không đúng" });
      }
    } else {
      res.status(404).send({ message: "Không tìm thấy username phù hợp" });
    }
  } catch (error) {
    res.status(500).send(error);
  }
};
const getAllUser = async (req, res) => {
  try {
    const usersList = await getAllUsersService();
    res.status(200).send(usersList);
  } catch (error) {
    res.status(500).send(error);
  }
};

const getAllUserPagination = async (req, res) => {
  try {
    const page = req.params.page;
    const offSet = (page - 1) * 4;
    const listUserPaginated = await getAllUserPaginationService(offSet);
    res.status(200).send(listUserPaginated);
  } catch (error) {
    res.status(500).send("Không thể lây danh sách người dùng");
  }
};

const getListSearchBaseOnInfo = async (req, res) => {
  const { info, page } = req.query;
  const offset = (page - 1) * 4;
  try {
    const listSearch = await getListSearchBaseOnInfoService(info, offset);
    res.status(200).send(listSearch);
  } catch (error) {
    res.status(500).send("Không tìm thấy dữ liệu");
  }
};

const getUserInfoById = async (req, res) => {
  const { id } = req.params;
  try {
    const user = await getUserInfoByIdService(id);
    res.status(200).send(user);
  } catch (error) {
    res.status(500).send(error);
  }
};
const deleteUserById = async (req, res) => {
  const { id } = req.params;

  try {
    await deleteUserByIdService(id);
    res.status(200).send("Xóa người dùng thành công");
  } catch (error) {
    res.status(500).send("Không thể xóa người dùng");
  }
};
const updateUserInfoById = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.status(422).json({ errors: errors.array() });
    return;
  }
  try {
    const { id } = req.params;
    const newUserInfo = req.body;
    const result = await updateUserInfoByIdService(newUserInfo, id);
    // Nếu thông tin của user update thành công thì sẽ lưu thông tin mới
    if (result.userUpdate) {
      res.status(200).send(newUserInfo);
    } else {
      res.status(401).send({ errors: [{ msg: result.error }] });
    }
  } catch (error) {
    res.status(500).send(error);
  }
};
module.exports = {
  register,
  login,
  getAllUser,
  deleteUserById,
  updateUserInfoById,
  getUserInfoById,
  getAllUserPagination,
  getListSearchBaseOnInfo,
};
