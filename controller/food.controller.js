const {
  createFoodService,
  getFoodByIdService,
  getAllFoodService,
  deleFoodByIdServices,
  updateFoodServices,
} = require('../services/food.service');
const { validationResult } = require('express-validator');
const createFood = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.status(422).json({ errors: errors.array() });
    return;
  }
  const foodReq = req.body;
  try {
    const newRestaurant = await createFoodService(foodReq);

    res.status(201).send(newRestaurant);
  } catch (error) {
    res.status(500).send(error);
  }
};
const getAllFood = async (req, res) => {
  try {
    const foodtList = await getAllFoodService();
    res.status(200).send(foodtList);
  } catch (error) {
    res.status(500).send(error);
  }
};
const getFoodById = async (req, res) => {
  const { id } = req.params;
  try {
    const food = await getFoodByIdService(id);
    res.status(200).send(food);
  } catch (error) {
    res.status(500).send(error);
  }
};
const deleteFoodById = async (req, res) => {
  const { id } = req.params;
  try {
    await deleFoodByIdServices(id);
    res.status(200).send('Xóa thành công');
  } catch (error) {
    res.status(500).send(error);
  }
};
const updateFood = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.status(422).json({ errors: errors.array() });
    return;
  }
  const { id } = req.params;
  const food = req.body;

  const foodupdate = await updateFoodServices(food, id);
  if (foodupdate) {
    res.status(200).send(foodupdate);
  } else {
    res.status(500).send('Dữ liệu không hợp lệ');
  }
};
module.exports = {
  createFood,
  getAllFood,
  getFoodById,
  deleteFoodById,
  updateFood,
};
