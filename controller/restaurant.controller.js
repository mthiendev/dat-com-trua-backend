const { validationResult } = require("express-validator");
const { restaurants } = require("../models");
const {
  getAllRestaurantsService,
  getRestaurantListService,
  deleteRestaurantByIdService,
  updateRestaurantByIdService,
  getRestaurantByIdService,
} = require("../services/restaurant.services");

const createRestaurant = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.status(422).json({ errors: errors.array() });
    return;
  }
  const { name, address, phoneNumber } = req.body;
  try {
    const newRestaurant = await restaurants.create({
      name,
      address,
      phoneNumber,
    });
    res.status(201).send(newRestaurant);
  } catch (error) {
    res.status(500).send(error);
  }
};
const getRestaurants = async (req, res) => {
  try {
    const restaurantList = await getAllRestaurantsService();
    res.status(200).send(restaurantList);
  } catch (error) {
    res.status(500).send(error);
  }
};
const getResraurantById = async (req, res) => {
  const { id } = req.params;
  try {
    const restaurant = await getRestaurantByIdService(id);
    res.status(200).send(restaurant);
  } catch (error) {
    res.status(500).send(error);
  }
};
const getRestaurantList = async (req, res) => {
  try {
    const result = await getRestaurantListService();
    res.status(200).send(result);
  } catch (error) {
    res.status(500).send(error);
  }
};
const deleteRestaurantById = async (req, res) => {
  const { id } = req.params;
  try {
    await deleteRestaurantByIdService(id);
    res.status(200).send("Xóa nhà hàng thành công");
  } catch (error) {
    res.status(500).send(error);
  }
};

const updateRestaurantById = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.status(422).json({ errors: errors.array() });
    return;
  }
  const { id } = req.params;
  const restaurant = req.body;
  try {
    const restaurantUpdate = await updateRestaurantByIdService(restaurant, id);
    if (restaurantUpdate) {
      res.status(200).send(restaurant);
    }
  } catch (error) {
    res.status(500).send(error);
  }
};

module.exports = {
  createRestaurant,
  getRestaurants,
  getResraurantById,
  getRestaurantList,
  deleteRestaurantById,
  updateRestaurantById,
};
