const socketIO = require("socket.io");
const createSocketIO = (httpServer) => {
  const io = socketIO(httpServer, {
    allowEIO3: true,
    cors: {
      origin: true,
      credentials: true,
    },
  });
  io.serveClient(true);
  io.on("connect", (socket) => {
    console.log("kết nối socket io");
    socket.on("sendCheckOutCart", function () {
      // nhận tín hiệu từ client
      socket.to().emit("message", "hello friends!");
      socket.emit("receiveCheckOutCart", "hello");
    });
  });
};

module.exports = { createSocketIO };
