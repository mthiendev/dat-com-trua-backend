const authorize = (arrtype) => (req, res, next) => {
  const { user } = req;
  const index = arrtype.findIndex((ele) => ele == user.type);
  if (index > -1) {
    next();
  } else {
    res.status(403).send("Bạn đã đăng nhập nhưng không có quyền");
  }
};
module.exports = { authorize };
