const { check } = require("express-validator");
const validateRegisterUser = () => {
  const vnf_regex =
    /^(032|033|034|035|036|037|038|039|086|096|097|098|081|082|083|084|085|088|091|094|056|058|092|070|076|077|078|079|089|090|093|099|059)+([0-9]{7})$/;
  return [
    check("fullName", "Tên không được để trống và không đước chứa chữ số")
      .not()
      .isEmpty()
      .isAlpha("vi-VN", { ignore: " " }),
    check("email", "Email không được để trống và phải đúng định dạng")
      .not()
      .isEmpty()
      .isEmail(),
    check("phoneNumber", "Số điện thoại không được để trống").not().isEmpty(),
    check("phoneNumber", "Số điện thoại không hợp lệ").custom(
      (value, { req }) => {
        return vnf_regex.test(value);
      }
    ),

    check("password", "Mật khẩu từ 6 - 36 kí tự ").isLength({
      min: 6,
      max: 36,
    }),
  ];
};
const validatorLoginUser = () => {
  return [
    check("email", "Email không được để trống và phải đúng định dạng")
      .not()
      .isEmpty()
      .isEmail(),
    check("password", "Mật khẩu từ 6 kí tự trở lên").isLength({ min: 6 }),
  ];
};
module.exports = { validateRegisterUser, validatorLoginUser };
