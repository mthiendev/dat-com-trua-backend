const { check } = require("express-validator");
const validateRestaurant = () => {
  const vnf_regex =
    /^(032|033|034|035|036|037|038|039|086|096|097|098|081|082|083|084|085|088|091|094|056|058|092|070|076|077|078|079|089|090|093|099|059)+([0-9]{7})$/;
  return [
    check("name", "name does not Empty").not().isEmpty(),
    check("address", "address does not Empty").not().isEmpty(),
    check("phoneNumber", "phoneNumbers does not Empty").not().isEmpty(),
    check("phoneNumber", "Số điện thoại không hợp lệ").custom(
      (value, { req }) => {
        return vnf_regex.test(value);
      },
    ),
  ];
};

module.exports = { validateRestaurant };
