const checkExist = (Model) => {
  return async (req, res, next) => {
    try {
      const { id } = req.params;
      if (!id) {
        throw new Error("Yêu cầu ID");
      }
      const model = await Model.findOne({ where: { id } });
      if (model) {
        next();
      } else {
        throw new Error(`Không tìm thấy id là ${id}`);
      }
    } catch (error) {
      res.status(404).send(error.message);
    }
  };
};
module.exports = { checkExist };
