const { check } = require("express-validator");
const validateFood = () => {
  const regex = /^(\$|)([1-9]\d{0,2}(\,\d{3})*|([1-9]\d*))(\.\d{2})?$/;
  return [
    check("name", "name does not Empty").not().isEmpty(),
    check("price", "Giá tiền không hợp lệ")
      .not()
      .isEmpty()
      .custom((value, { req }) => {
        return value >= 1000;
      }),
    check("restaurantId", "restaurantId does not Empty").not().isEmpty(),
    check("quantity", "số lượng phải lớn hơn 0")
      .not()
      .isEmpty()
      .custom((value, { req }) => {
        return regex.test(value);
      }),
  ];
};

module.exports = { validateFood };
