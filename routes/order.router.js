const express = require("express");
const {
  createOrder,
  deleteOrder,
  getOrders,
  updateOrderStatus,
  getOrdersByOrderId,
} = require("../controller/order.controller");
const { authenticate } = require("../middlewares/auth/authenticate");
const { authorize } = require("../middlewares/auth/autthorize");
const { checkExist } = require("../middlewares/validations/checkExist");
const { Orders } = require("../models");

const orderRouter = express.Router();

orderRouter.post("/", authenticate, createOrder);
orderRouter.get("/", authenticate, getOrders);
orderRouter.get("/:id", checkExist(Orders), authenticate, getOrdersByOrderId);
orderRouter.delete("/:id", checkExist(Orders), authenticate, deleteOrder);
orderRouter.put(
  "/:id",
  checkExist(Orders),
  authenticate,
  authorize(["admin"]),
  updateOrderStatus
);

module.exports = orderRouter;
