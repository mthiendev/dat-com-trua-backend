var express = require('express');
const foodRouter = require('./food.router');
const orderRouter = require('./order.router');
const OrdersFoodsRouter = require('./orders_foods.router');
const restaurantRouter = require('./restaurant.router');
const userRouter = require('./user.router');

var rootRouter = express.Router();

rootRouter.use('/restaurants', restaurantRouter);
rootRouter.use('/foods', foodRouter);
rootRouter.use('/users', userRouter);
rootRouter.use('/orders', orderRouter);
rootRouter.use('/orders-foods', OrdersFoodsRouter);

module.exports = rootRouter;
