const express = require("express");
const {
  createOrdersFoods,
  deleteOrdersFoods,
} = require("../controller/Orders_Foods.controller");
const { authenticate } = require("../middlewares/auth/authenticate");
const { checkExist } = require("../middlewares/validations/checkExist");
const { OrdersFoods } = require("../models");

const OrdersFoodsRouter = express.Router();

OrdersFoodsRouter.post("/", createOrdersFoods);
OrdersFoodsRouter.delete("/:id", checkExist(OrdersFoods), deleteOrdersFoods);
OrdersFoodsRouter.post("/creatOrdersFoods", authenticate, createOrdersFoods);

module.exports = OrdersFoodsRouter;
