const express = require("express");
const {
  register,
  login,
  getAllUser,
  deleteUserById,
  updateUserInfoById,
  getUserInfoById,
  getAllUserPagination,
  getListSearchBaseOnInfo,
} = require("../controller/user.controller");
const { authenticate } = require("../middlewares/auth/authenticate");
const { authorize } = require("../middlewares/auth/autthorize");
const { checkExist } = require("../middlewares/validations/checkExist");
const {
  validateRegisterUser,
  validatorLoginUser,
} = require("../middlewares/validations/user.validator");
const { Users } = require("../models");

const userRouter = express.Router();

userRouter.get("/", getAllUser);
userRouter.get("/getListSearch", getListSearchBaseOnInfo);
userRouter.get("/:page", getAllUserPagination);

userRouter.put(
  "/update/:id",
  checkExist(Users),
  authenticate,
  validateRegisterUser(),
  updateUserInfoById
);

userRouter.post("/register", validateRegisterUser(), register);
userRouter.post("/login", validatorLoginUser(), login);
userRouter.get(
  "/userInfo/:id",
  checkExist(Users),
  authenticate,
  getUserInfoById
);
userRouter.delete(
  "/:id",
  checkExist(Users),
  authenticate,
  authorize(["admin"]),
  deleteUserById
);

module.exports = userRouter;
