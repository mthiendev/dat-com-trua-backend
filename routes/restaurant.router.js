const express = require("express");
const {
  createRestaurant,
  getRestaurants,
  getResraurantById,
  getRestaurantList,
  deleteRestaurantById,
  updateRestaurantById,
} = require("../controller/restaurant.controller");
const { authenticate } = require("../middlewares/auth/authenticate");
const { authorize } = require("../middlewares/auth/autthorize");
const { checkExist } = require("../middlewares/validations/checkExist");
const {
  validateRestaurant,
} = require("../middlewares/validations/restaurant.validator");
const { restaurants } = require("../models");

const restaurantRouter = express.Router();

restaurantRouter.post("/", validateRestaurant(), createRestaurant);
restaurantRouter.get("/", getRestaurants);
restaurantRouter.get("/all-restaurant", getRestaurantList);
restaurantRouter.get("/:id", checkExist(restaurants), getResraurantById);
restaurantRouter.delete(
  "/:id",
  checkExist(restaurants),
  authenticate,
  authorize(["admin"]),
  deleteRestaurantById,
);
restaurantRouter.put(
  "/update/:id",
  checkExist(restaurants),
  authenticate,
  authorize(["admin"]),
  validateRestaurant(),
  updateRestaurantById,
);

module.exports = restaurantRouter;
