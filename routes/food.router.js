const express = require("express");
const {
  createFood,
  getAllFood,
  getFoodById,
  deleteFoodById,
  updateFood,
} = require("../controller/food.controller");
const { checkExist } = require("../middlewares/validations/checkExist");

const { Foods } = require("../models");
const { validateFood } = require("../middlewares/validations/food.validatior");
const { authenticate } = require("../middlewares/auth/authenticate");
const { authorize } = require("../middlewares/auth/autthorize");
const foodRouter = express.Router();
foodRouter.post(
  "/",
  authenticate,
  authorize(["admin"]),
  validateFood(),
  createFood,
);
foodRouter.get("/", getAllFood);
foodRouter.get("/:id", checkExist(Foods), getFoodById);
foodRouter.delete(
  "/:id",
  authenticate,
  authorize(["admin"]),
  checkExist(Foods),
  deleteFoodById,
);
foodRouter.put(
  "/:id",
  authenticate,
  authorize(["admin"]),
  checkExist(Foods),
  validateFood(),
  updateFood,
);

module.exports = foodRouter;
